package part5_advanced

import akka.actor.ActorSystem
import akka.stream.KillSwitches
import akka.stream.scaladsl.{BroadcastHub, Keep, MergeHub, Sink, Source}

import scala.concurrent.duration.DurationInt

object DynamicStreamHandling extends App {
  /*
   * Goal
   *  Stop or abort a stream at runtime
   *  Dynamically add fan-in/fan-out branches
   */

  implicit private val system: ActorSystem = ActorSystem("DynamicStreamHandling")

  import system.dispatcher

  // #1: Kill Switch
  /*
   * a special kind of flow that emits the same elements that go through it,
   * but materializes to a special value that has some additional methods.
   */

  // single kill switch
  private val killSwitchFlow = KillSwitches.single[Int]
  private val counter = Source(LazyList.from(1)).throttle(1, 1.second).log("counter")
  private val sink = Sink.ignore

  /*private val killSwitch = counter
    .viaMat(killSwitchFlow)(Keep.right)
    .to(sink)
    .run()

  system.scheduler.scheduleOnce(3.seconds) {
    killSwitch.shutdown()
  }*/

  // shared kill switch
  private val anotherCounter = Source(LazyList.from(1)).throttle(2, 1.second).log("anotherCounter")
  private val sharedKillSwitch = KillSwitches.shared("oneButtonToRuleThemAll")

  /*counter.via(sharedKillSwitch.flow).runWith(sink)
  anotherCounter.via(sharedKillSwitch.flow).runWith(sink)*/

  system.scheduler.scheduleOnce(3.seconds) {
    /*sharedKillSwitch.shutdown()*/
  }

  // #2: MergeHub
  /*
   * a special component that allows to dynamically at runtime
   * add virtual fan-in components to the same consumer
   *
   * in the end we plug all these sources to the same consumer
   *
   * Advantage:
   *  - it can be programmed to run while the stream is active
   *    unlike like GraphDSL
   */
  // materializing to a sink because it has inputs
  private val dynamicMerge = MergeHub.source[Int]
  private val materializedSink = dynamicMerge.to(Sink.foreach[Int](println)).run()

  // use the sink any time we like
  /*Source(1 to 10).runWith(materializedSink)
  counter.runWith(materializedSink)*/

  // #3: BroadcastHub
  // materializing to a source because it has outputs
  private val dynamicBroadcast = BroadcastHub.sink[Int]
  private val materializedSource = Source(1 to 100).runWith(dynamicBroadcast)

  // use the source any number of times in any graphs/components
  /*materializedSource.runWith(sink)
  materializedSource.runWith(Sink.foreach[Int](println))*/

  /*
   * Challenge - combine a mergeHub and a broadcastHub.
   *
   * A publisher-subscriber component
   *  - you can dynamically add sources and sinks to this component and
   *    every single element produced by every single source will be
   *    known by every single subscriber.
   */
  private val merge = MergeHub.source[String]
  private val broadcast = BroadcastHub.sink[String]
  private val (publisherPort, subscriberPort) = merge.toMat(broadcast)(Keep.both).run()

  subscriberPort.runWith(Sink.foreach(e => println(s"I received: $e")))
  subscriberPort.map(string => string.length).runWith(Sink.foreach(n => println(s"I got a number: $n")))

  Source(List("Akka", "is", "amazing")).runWith(publisherPort)
  Source(List("I", "love", "Scala")).runWith(publisherPort)
  Source.single("STREEEAMS").runWith(publisherPort)
}
