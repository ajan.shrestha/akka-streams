package part5_advanced

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Balance, GraphDSL, Merge, RunnableGraph, Sink, Source}
import akka.stream.{ClosedShape, Graph, Inlet, Outlet, Shape}

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

object CustomGraphShapes extends App {
  /*
   * Goal
   *  Create components with arbitrary inputs and outputs and of arbitrary types
   *
   *  Example:
   *    - Create a very generic balance component
   *      An M x N Balance component (any num of i/ps and o/ps)
   *    - This component would feed a relatively equal number of elements
   *      to each of its N outputs, regardless of the rate of production
   *      of its M inputs.
   *
   *
   *  [ ]                   [ ]             |      [ ]         [ ]     |
   *  [ ] ->            ->  [ ] ->          |  ->  [ ]         [ ] ->  |
   *  [ ] ->            ->  [ ] ->          |  ->  [ ] ->  ->  [ ] ->  |
   *  [ ] ->            ->  [ ] ->          |  ->  [ ]         [ ] ->  |
   *  [ ]                   [ ]             |      [ ]         [ ]     |
   * normal balance     desired balance     potential implementation = Merge + normal Balance
   *
   *  GraphDSL to the rescue
   *  Problem: there is no predefined shape
   */
  implicit private val system: ActorSystem = ActorSystem("CustomGraphShapes")

  // balance 2x3 shape
  // case class to expose ports as members
  private case class Balance2x3(
                                 in0: Inlet[Int],
                                 in1: Inlet[Int],
                                 out0: Outlet[Int],
                                 out1: Outlet[Int],
                                 out2: Outlet[Int],
                               ) extends Shape {
    // Every shape that we want to include in a graph
    // needs to have our ports exposed as instances of special classes.
    // Inlet[T], Outlet[T] // classes for the ports.
    // these ports should return in defined order
    override val inlets: Seq[Inlet[_]] = List(in0, in1)

    override val outlets: Seq[Outlet[_]] = List(out0, out1, out2)

    override def deepCopy(): Shape = Balance2x3(
      in0.carbonCopy(),
      in1.carbonCopy(),
      out0.carbonCopy(),
      out1.carbonCopy(),
      out2.carbonCopy(),
    )
  }

  private val balance2x3Impl = GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._

    val merge = builder.add(Merge[Int](2))
    val balance = builder.add(Balance[Int](3))

    merge ~> balance

    Balance2x3(
      merge.in(0),
      merge.in(1),
      balance.out(0),
      balance.out(1),
      balance.out(2),
    )
  }

  private val balance2x3Graph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val slowSource = Source(LazyList.from(1)).throttle(1, 1.second)
      val fastSource = Source(LazyList.from(1)).throttle(2, 1.second)

      def createSink(index: Int): Sink[Int, Future[Int]] =
        Sink.fold(0)((count: Int, element: Int) => {
          println(s"[sink $index] Received: $element, current count is $count")
          count + 1
        })

      val sink1 = builder.add(createSink(1))
      val sink2 = builder.add(createSink(2))
      val sink3 = builder.add(createSink(3))

      val balance2x3 = builder.add(balance2x3Impl)

      slowSource ~> balance2x3.in0
      fastSource ~> balance2x3.in1

      balance2x3.out0 ~> sink1
      balance2x3.out1 ~> sink2
      balance2x3.out2 ~> sink3

      ClosedShape
    }
  )

  // balance2x3Graph.run()

  /*
   * Exercise: generalize the balance component, M x N
   *  - take in generic type
   */
  private case class BalanceMxN[T](
                                    override val inlets: List[Inlet[T]],
                                    override val outlets: List[Outlet[T]],
                                  ) extends Shape {
    override def deepCopy(): Shape = BalanceMxN(
      inlets.map(_.carbonCopy()),
      outlets.map(_.carbonCopy()),
    )
  }

  private object BalanceMxN {
    def apply[T](inputCount: Int, outputCount: Int): Graph[BalanceMxN[T], NotUsed] =
      GraphDSL.create() { implicit builder =>
        import GraphDSL.Implicits._

        val merge = builder.add(Merge[T](inputCount))
        val balance = builder.add(Balance[T](outputCount))

        merge ~> balance

        BalanceMxN(
          merge.inlets.toList,
          balance.outlets.toList,
        )
      }
  }

  private val balanceMxNGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val slowSource = Source(LazyList.from(1)).throttle(1, 1.second)
      val fastSource = Source(LazyList.from(1)).throttle(2, 1.second)

      def createSink(index: Int): Sink[Int, Future[Int]] =
        Sink.fold(0)((count: Int, element: Int) => {
          println(s"[sink $index] Received: $element, current count is $count")
          count + 1
        })

      val sink1 = builder.add(createSink(1))
      val sink2 = builder.add(createSink(2))
      val sink3 = builder.add(createSink(3))

      val balanceMxN = builder.add(BalanceMxN[Int](2, 3))

      slowSource ~> balanceMxN.inlets.head
      fastSource ~> balanceMxN.inlets(1)

      balanceMxN.outlets.head ~> sink1
      balanceMxN.outlets(1) ~> sink2
      balanceMxN.outlets(2) ~> sink3

      ClosedShape
    }
  )

  balanceMxNGraph.run()
}
