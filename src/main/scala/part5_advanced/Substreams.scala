package part5_advanced

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Keep, Sink, Source}

import scala.util.{Failure, Success}

object Substreams extends App {
  /*
   * Goal
   *  Create streams dynamically as they're running
   *  Process substreams uniformly
   */
  implicit private val system: ActorSystem = ActorSystem("Substreams")

  import system.dispatcher

  // 1 -  grouping a stream by a certain function
  private val wordSource = Source(List("Akka", "is", "amazing", "learning", "substreams"))
  // stream of streams
  private val groups = wordSource
    .groupBy(30, word => if (word.isEmpty) '\u0000' else word.toLowerCase().charAt(0))

  groups.to(Sink.fold(0)((count, word) => {
    val newCount = count + 1
    println(s"I just received $word, count is $newCount")
    newCount
  }))
    .run()

  // 2 - merge substreams back
  private val textSource = Source(List(
    "I love Akka Streams",
    "this is amazing",
    "learning from Rock the JVM",
  ))

  private val totalCharCountFuture = textSource
    .groupBy(2, string => string.length % 2)
    .map(_.length) // do your expensive computation here
    .mergeSubstreams //WithParallelism(2)
    .toMat(Sink.reduce[Int](_ + _))(Keep.right)
    .run()

  totalCharCountFuture.onComplete {
    case Success(value) => println(s"Total char count: $value")
    case Failure(ex) => println(s"Char computation failed: $ex")
  }

  // 3 - splitting a stream into substream, when a condition is met
  private val text =
    "I love Akka Streams\n" +
      "this is amazing\n" +
      "learning from Rock the JVM\n"
  private val anotherCharCountFuture = Source(text.toList)
    .splitWhen(c => c == '\n')
    .filter(_ != '\n')
    .map(_ => 1)
    .mergeSubstreams
    .toMat(Sink.reduce[Int](_ + _))(Keep.right)
    .run()

  anotherCharCountFuture.onComplete {
    case Success(value) => println(s"Total char count alternative: $value")
    case Failure(ex) => println(s"Char computation failed: $ex")
  }

  /*
   * Flattening
   *  Each element can spin up its own stream
   *  Choose how to flatten
   *    - concatenate : waiting for one upstream to finish before taking on the next one
   *    - merge       : elements from all sub streams are taken in no defined order
   */
  private val simpleSource = Source(1 to 5)
  simpleSource.flatMapConcat(x => Source(x to (3 * x))).runWith(Sink.foreach(println))
  simpleSource.flatMapMerge(2, x => Source(x to (3 * x))).runWith(Sink.foreach(println))
}
