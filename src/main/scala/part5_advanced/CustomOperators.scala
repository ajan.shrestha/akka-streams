package part5_advanced

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.stream.{Attributes, FlowShape, Inlet, Outlet, SinkShape, SourceShape}
import akka.stream.stage.{GraphStage, GraphStageLogic, GraphStageWithMaterializedValue, InHandler, OutHandler}

import scala.collection.mutable
import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Random, Success}

object CustomOperators extends App {
  /*
   * Goal
   *  Create your own components(operators) with custom logic
   *    - custom cogs
   *      - process data
   *      - create threads
   *      - open TCP connections, ...
   *  Master the GraphStage API
   *
   *  Input port methods
   *    - InHandlers interact with the upstream
   *      - onPush // when we receive an element on that input port
   *      - onUpstreamFinish // when the upstream terminates the stream
   *      - onUpstreamFailure // when the upstream throws an error
    * Input ports can check and retrieve elements (manually)
    *   - pull: signal demand from upstream
    *   - grab: takes the element and it will fail if there is no element to grab
    *   - cancel: tell upstream to stop/terminate
    *   - isAvailable: checking whether the input port is available
    *                   whether an element has been pushed to the port
    *   - hasBeenPulled: whether demand has already been signaled on that part
    *   - isClosed
    *
    * Output port methods
    *   - OutHandlers interact with downstream
    *     - onPull // mandatory; called when demand has been signaled on this port
    *     - onDownstreamFinish
    *     - (no equivalent onDownstreamFailure for the failure callback because
    *       if downstream fails, I'll receive a cancel signal)
    * Output port can send elements (manually)
    *   - push: send an element
    *   - complete: finish the stream
    *   - fail: terminates the stream with an exception
    *   - isAvailable: which check if an element has already been pushed to the port
    *   - isClosed
    *
    * NOTE:
    *   - Handler callbacks onPush and onPull are never, ever called concurrently
    *     hence can safely access mutable state inside these handlers
    *   - (Downside) never, ever expose mutable state outside these handlers
    *     example: in future onComplete callbacks
    *       - it breaks the component encapsulation
   */
  implicit private val system: ActorSystem = ActorSystem("CustomOperators")

  // 1 - a custom source which emits random numbers until cancelled

  private class RandomNumberGenerator(max: Int)
    extends GraphStage[ /* step 0: define the shape */ SourceShape[Int]] {

    // step 1: define the ports and the component-specific members
    private val outPort = Outlet[Int]("randomGenerator")
    private val random = new Random()

    // step 2: construct a new shape
    override def shape: SourceShape[Int] = SourceShape(outPort)

    // step 3: create the logic
    override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
      new GraphStageLogic(shape) {
        // ste 4:
        // define mutable logic
        // implement my logic here

        // set handlers on our ports
        setHandler(outPort, new OutHandler {
          // when there is demand from downstream
          override def onPull(): Unit = {
            // emmit a new element
            val nextNumber = random.nextInt(max)
            // push it out of the outPort
            push(outPort, nextNumber)
          }
        })
      }
  }

  private val randomGeneratorSource = Source.fromGraph(new RandomNumberGenerator(100))
  // randomGeneratorSource.runWith(Sink.foreach(println))

  // 2 - a custom sink that prints elements in batches of a given size
  private class Batcher(batchSize: Int) extends GraphStage[SinkShape[Int]] {
    private val inPort = Inlet[Int]("batcher")

    override def shape: SinkShape[Int] = SinkShape[Int](inPort)

    override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) {

      // lifecycle: send the signal
      override def preStart(): Unit = {
        pull(inPort)
      }

      // mutable state
      val batch = new mutable.Queue[Int]

      setHandler(inPort, new InHandler {
        // when the upstream wants to send me an element
        override def onPush(): Unit = {
          val nextElement = grab(inPort)
          batch.enqueue(nextElement)

          // assume some complex computation
          Thread.sleep(100)

          if (batch.size >= batchSize) {
            println("New batch: " + batch.dequeueAll(_ => true).mkString("[", ", ", "]"))
          }

          pull(inPort) // send demand upstream
        }

        override def onUpstreamFinish(): Unit = {
          if (batch.nonEmpty) {
            println("New batch: " + batch.dequeueAll(_ => true).mkString("[", ", ", "]"))
            println("Stream finished.")
          }
        }
      })
    }
  }

  private val batcherSink = Sink.fromGraph(new Batcher(10))
  // randomGeneratorSource.to(batcherSink).run()1

  /*
   * Exercise:: a custom flow - a simple filter flow
   * - 2 ports: an input port and an output port
   */

  private class SimpleFilter[T](predicate: T => Boolean) extends GraphStage[FlowShape[T, T]] {
    private val inPort = Inlet[T]("filterIn")
    private val outPort = Outlet[T]("filterOut")

    override def shape: FlowShape[T, T] = FlowShape(inPort, outPort)

    override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
      new GraphStageLogic(shape) {
        setHandler(outPort, new OutHandler {
          override def onPull(): Unit = pull(inPort)
        })

        setHandler(inPort, new InHandler {
          override def onPush(): Unit = {
            try {
              val nextElement = grab(inPort)

              if (predicate(nextElement)) push(outPort, nextElement) // past it on
              else pull(inPort) // ask for another element
            } catch {
              case e: Throwable => failStage(e)
            }
          }
        })
      }
  }

  private val myFilter = Flow.fromGraph(new SimpleFilter[Int](_ > 50))
  randomGeneratorSource
    .via(myFilter)
    .to(batcherSink)
  //.run()
  // backpressure

  /*
   * Materialized values in graph stages
   */
  // 3 - a flow that counts the number of elements that go through it
  private class CounterFlow[T]() extends GraphStageWithMaterializedValue[FlowShape[T, T], Future[Int]] {
    private val inPort = Inlet[T]("counterIn")
    private val outPort = Outlet[T]("counterOut")

    override val shape: FlowShape[T, T] = FlowShape(inPort, outPort)

    override def createLogicAndMaterializedValue(inheritedAttributes: Attributes): (GraphStageLogic, Future[Int]) = {
      val promise = Promise[Int]
      val logic: GraphStageLogic = new GraphStageLogic(shape) {
        // setting mutable state
        var counter = 0

        setHandler(outPort, new OutHandler {
          override def onPull(): Unit = pull(inPort)

          override def onDownstreamFinish(cause: Throwable): Unit = {
            promise.success(counter)
            super.onDownstreamFinish(cause)
          }
        })

        setHandler(inPort, new InHandler {
          override def onPush(): Unit = {
            // extract the element
            val nextElement = grab(inPort)
            counter += 1
            push(outPort, nextElement)
          }

          override def onUpstreamFinish(): Unit = {
            promise.success(counter)
            super.onUpstreamFinish()
          }

          override def onUpstreamFailure(ex: Throwable): Unit = {
            promise.failure(ex)
            super.onUpstreamFailure(ex)
          }
        })
      }

      (logic, promise.future)
    }
  }

  private val counterFlow = Flow.fromGraph(new CounterFlow[Int])
  private val countFuture = Source(1 to 10)
    //.map(x => if (x == 7) throw new RuntimeException("gotcha!") else x)
    .viaMat(counterFlow)(Keep.right)
    .to(Sink.foreach[Int](x => if (x == 7) throw new RuntimeException("gotcha!") else println(x)))
    //.to(Sink.foreach[Int](println))
    .run()

  import system.dispatcher

  countFuture.onComplete {
    case Success(count) => println(s"The number of elements passed: $count")
    case Failure(ex) => println(s"Counting the elements failed: $ex")
  }
}
