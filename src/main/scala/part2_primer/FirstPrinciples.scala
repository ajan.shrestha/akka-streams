package part2_primer

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.concurrent.Future

object FirstPrinciples extends App {

  /*
   * Goal
   *  - learn how to understand, read and design
   *    - asynchronous,
   *    - back-pressured,
   *    - incremental,
   *    - potentially infinite data processing systems
   *  known as reactive streams
   *
   *  Reactive Streams
   *    - https://www.reactive-streams.org/
   *    - an initiative started by many companies and teams
   *      of engineers throughout the world
   *    - Specification of async, back-pressured streams
   *
   *  Concepts:
   *    - publisher = a component that emits elements{asynchronously)
   *    - subscriber = receives elements
   *    - processor = (transformer) transforms elements along the way
   *    - async = evaluated or executed at no well-defined time (non-blocking)
   *    - backpressure
   *
   *  Reactive Streams is an SPI (service provider interface), not an API
   *    - it defines the concepts and how they work, including the protocols
   *      between all these components.
   *    - But libraries that implement the reactive stream specification have the
   *      power to choose their own API
   *
   *  Our Focus: The Akka Streams API
   */

  /*
   * Akka Streams
   *  Components
   *    - Source = "publisher"
   *      - emits elements asynchronously
   *      - may or may not terminate
   *    - Sink = "subscriber"
   *      - receives elements
   *      - terminates only when the publisher terminates
   *    - Flow = "processor"
   *      - transforms elements
   *  We build streams by connecting components
   *
   *  Directions
   *    - upstream = to the source
   *    - downstream = to the sink
   *
   *  [Source] -> [Flow] -> [Sink]
   *  <= upstream    downstream =>
   */

  // ActorSystem handles the implicit materializer
  implicit private val system: ActorSystem = ActorSystem("FirstPrinciples")
  // Materializer allows the running of Akka Streams
  // private val materializer = ActorMaterializer()

  // sources
  private val source = Source(1 to 10)
  // sinks
  private val sink = Sink.foreach[Int](println)

  // definition of an akka stream
  // graph is the expression Source.to(Sink)
  private val graph = source.to(sink)
  // graph.run()

  // flows transform elements
  private val flow = Flow[Int].map(x => x + 1)
  private val sourceWithFlow = source.via(flow)
  private val flowWithSink = flow.to(sink)

  // sourceWithFlow.to(sink).run()
  // source.to(flowWithSink).run()
  // source.via(flow).to(sink).run()

  // Sources can emit any kind of objects as long as
  // they're immutable and serializable, like actor messages
  // because these components are based on actors.

  // nulls are NOT allowed per Reactive Streams Specifications
  /*
  private val illegalSource = Source.single[String](null)
  illegalSource.to(Sink.foreach(println)).run()
  */
  // use Options instead

  // various kinds of sources
  // Many sources are finite
  private val finiteSource = Source.single(1)
  private val anotherFiniteSource = Source(List(1, 2, 3))
  // empty source that never emits elements i.e. terminated before it starts
  private val emptySource = Source.empty[Int]
  private val infiniteSource = Source(LazyList.from(1)) // do not confuse an Akka stream with a "collection" Stream

  import scala.concurrent.ExecutionContext.Implicits.global

  private val futureSource = Source.future(Future(42)) // source will emit value 42 when the future is completed

  // sinks
  private val theMostBoringSink = Sink.ignore
  private val foreachSink = Sink.foreach[String](println)
  // there are sinks that retrieve and may return a value
  private val headSink = Sink.head[Int] // retrieves head and then closes the stream
  private val foldSink = Sink.fold[Int, Int](0)((a, b) => a + b)

  // flows = usually mapped to collection operators
  private val mapFlow = Flow[Int].map(x => 2 * x)
  private val takeFlow = Flow[Int].take(5) // turns stream into a finite stream
  // drop, filter
  // DO NOT have flatMap

  // Formal way to create stream
  // source -> flow -> flow -> .... -> sink
  private val doubleFlowGraph = source.via(mapFlow).via(takeFlow).to(sink)
  // doubleFlowGraph.run()

  // syntactic sugars
  private val mapSource = Source(1 to 10).map(x => x * 2) // Source(1 to 10).via(Flow[Int].map(x => x * 2))
  // run streams directly
  // mapSource.runForeach(println) // mapSource.to(Sink.foreach[Int](println)).run()

  // OPERATORS = components

  /*
   * Exercise: create a stream that takes the names of persons,
   *  then you will keep the first 2 names with length > 5 characters
   */

  private val names = List("Alice", "Bob", "Charlie", "David", "Martin", "AkkaStreams")
  private val nameSource = Source(names)
  private val longNameFlow = Flow[String].filter(name => name.length > 5)
  private val limitFlow = Flow[String].take(2)
  private val nameSink = Sink.foreach[String](println)
  // nameSource.via(longNameFlow).via(limitFlow).to(nameSink).run()
  nameSource.filter(_.length > 5).take(2).runForeach(println)
}
