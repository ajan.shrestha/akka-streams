package part2_primer

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}

import scala.util.{Failure, Success}

object MaterializingStreams extends App {

  /*
   * Goal
   *  - extract meaningful value out of a running stream
   *
   * Materializing
   *  - Components are static until they run
   *  - value obtained of the graph => materialized value of the graph
   *
   * A graph is a "blueprint" for a stream
   * Running a graph allocates the right resources
   *  - instantiating actors, allocating thread pools
   *  - sockets, connections
   *  - etc - everything is transparent
   *
   * Running a graph = materializing
   *  result -> materialized value of the graph
   *
   * Materialized Values
   *  Materializing a graph = materializing all components
   *    - each component produces a materialized value when run
   *    - the graph produces a single materialized value
   *    - our job to choose which one to pick
   *
   * A component can materialize multiple times
   *  - you can reuse the same component in different graphs
   *  - different runs = different materializations!
   *
   * A materialized value can be ANYTHING.
   *  - the materialized value may nor may not have any connection at all
   *    to the actual elements that go through the stream.
   */

  implicit private val system: ActorSystem = ActorSystem("MaterializingStreams")

  private val simpleGraph = Source(1 to 10).to(Sink.foreach(println))
  // private val simpleMaterializedValue = simpleGraph.run()

  import system.dispatcher

  private val source = Source(1 to 10)
  private val sink = Sink.reduce[Int]((a, b) => a + b)
  private val sumFuture = source.runWith(sink)
  /*sumFuture.onComplete {
    case Success(value) => println(s"The sum of all elements is $value")
    case Failure(exception) => println(s"The sum of the elements could not be computed: $exception")
  }*/

  // by default the left most value of the graph is kept as materialized value
  // choosing materialized values
  private val simpleSource = Source(1 to 10)
  private val simpleFlow = Flow[Int].map(x => x + 1)
  private val simpleSink = Sink.foreach[Int](println)
  // viaMat method takes the component that I want to connect to the source
  // and as another argument list, we can supply a combination function
  // that takes dematerialized values of the left components and
  // returns a third materialized value, which will be the materialized value
  // of the composite component
  // simpleSource.viaMat(simpleFlow)((sourceMat, flowMat) => flowMat)
  /*simpleSource.viaMat(simpleFlow)(Keep.left)
  simpleSource.viaMat(simpleFlow)(Keep.both)
  simpleSource.viaMat(simpleFlow)(Keep.none)*/
  private val graph = simpleSource.viaMat(simpleFlow)(Keep.right).toMat(simpleSink)(Keep.right)
  graph.run().onComplete {
    case Success(_) => println("Stream processing finished.")
    case Failure(exception) => println(s"Stream processing failed with: $exception")
  }

  // sugars
  // this takes the sink materialized value -> Keep.right
  private val sum = Source(1 to 10).runWith(Sink.reduce[Int](_ + _)) // source.to(Sink.reduce)(Keep.right)
  private val sum1 = Source(1 to 10).runReduce[Int](_ + _) // same

  // backwards
  Sink.foreach[Int](println).runWith(Source.single(42)) /// source(...).to(sink...).run()

  // both ways
  Flow[Int].map(x => 2 * x).runWith(simpleSource, simpleSink)

  /*
   * - return the last element of a source (use Sink.last)
   * - compute the total word out of a stream of sentences
   *    - map, fold, reduce
   */
  private val f1 = Source(1 to 10).toMat(Sink.last)(Keep.right).run()
  private val f2 = Source(1 to 10).runWith(Sink.last)

  private val sentenceSource = Source(List(
    "Akka is awesome",
    "I love streams",
    "Materialized values are killing me"
  ))
  private val wordsCountSink = Sink
    .fold[Int, String](0)(
      (currentWords, newSentence) =>
        currentWords + newSentence.split(" ").length)
  private val g1 = sentenceSource.toMat(wordsCountSink)(Keep.right).run()
  private val g2 = sentenceSource.runWith(wordsCountSink)
  private val g3 = sentenceSource.runFold(0)(
    (currentWords, newSentence) =>
      currentWords + newSentence.split(" ").length)

  private val wordCountFlow = Flow[String].fold[Int](0)(
    (currentWords, newSentence) =>
      currentWords + newSentence.split(" ").length)
  private val g4 = sentenceSource.via(wordCountFlow).toMat(Sink.head)(Keep.right).run()
  private val g5 = sentenceSource.viaMat(wordCountFlow)(Keep.left).toMat(Sink.head)(Keep.right).run()
  private val g6 = sentenceSource.via(wordCountFlow).runWith(Sink.head)
  private val g7 = wordCountFlow.runWith(sentenceSource, Sink.head)._2 // result is tuple
}
