package part2_primer

import akka.actor.ActorSystem
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source}

object BackpressureBasics extends App {

  /*
   * Backpressure
   *  - One of the fundamental features of Reactive Streams
   *
   *  Based on key ideas
   *    - Elements flow as response to demand from consumers
   *      Consumers are the actually the ones who triggers the flow
   *      of elements through a stream
   *
   *    Backpressure is all about the synchronization of speed
   *    in between these asynchronous components.
   *
   *  Fast consumers
   *    - all is well in Akka as consumers are processing elements
   *      as soon as they are available
   *  Slow consumers
   *    - problem because the upstream is producing elements faster
   *      than the consumer is able to process them
   *    - A special protocol will kick in
   *      consumer will send a signal to producer(upstream) to slow down
   *      -> limit the rate of production at source
   *      => This protocol is known as backpressure (is transparent)
   */

  implicit private val system: ActorSystem = ActorSystem("BackpressureBasics")

  private val fastSource = Source(1 to 1000)
  private val slowSink = Sink.foreach[Int] { x =>
    // simulate a long processing
    Thread.sleep(1000)
    println(s"Sink: $x")
  }

  // this not backpressure as it's run on a single actor
  /*fastSource.to(slowSink).run()*/
  // fusing?!

  /*fastSource.async.to(slowSink).run()*/
  // backpressure

  private val simpleFlow = Flow[Int].map { x =>
    println(s"Incoming: $x")
    x + 1
  }

  /*
   * Slow sink because it's slow, it sends backpressure signals upstream
   * Now, the simpleFlow, when it receives the backpressure signal,
   * instead of forwarding that backpressure signal to the source, it
   * buffers internally a number of elements until it's had enough.
   * The default buffer in Akka streams is 16 elements.
   * It forwards on buffer full to source until sink demands for some elements.
   * So, it releases in batches and again buffers.
   */

  /*fastSource.async
    .via(simpleFlow).async
    .to(slowSink)
    .run()*/

  /*
   * reactions to backpressure (in order):
   *  - try to slow down if possible
   *  - buffer elements until there's more demand
   *  - drop down elements from the buffer if it overflows
   *      - Akka stream programmers can only control what happens
   *        to a component at this point if the component has buffered
   *        enough elements and is about to overflow
   *  - tear down/kill the whole stream (failure)
   */

  // private val bufferedFlow = simpleFlow.buffer(10, overflowStrategy = OverflowStrategy.dropHead)
  // private val bufferedFlow = simpleFlow.buffer(10, overflowStrategy = OverflowStrategy.dropTail)
  // private val bufferedFlow = simpleFlow.buffer(10, overflowStrategy = OverflowStrategy.dropBuffer)
  private val bufferedFlow = simpleFlow.buffer(10, overflowStrategy = OverflowStrategy.backpressure)
  // private val bufferedFlow = simpleFlow.buffer(10, overflowStrategy = OverflowStrategy.fail)
  /*
   * OverflowStrategies
   *  - dropHead -> drop the oldest element in the buffer to make room for the new one
   *      Sink buffers default(16) elements
   *      Flow buffers the size mentioned in strategy (last few elements
   *  - dropTail -> drops the newest element in the buffer
   *  - drop -> the entire buffer
   *  - backpressure -> signal
   *  - fail
   */

  fastSource.async
    .via(bufferedFlow).async
    .to(slowSink)
    .run()

  /*
   * 1-16:    nobody is backpressured
   * 17-26:   flow will buffer, flow will start dropping at the next element
   * 26-1000: flow will always drop the oldest element
   *  => 991-1000 => 992-1001 => sink
   */

  /*
   * A back pressure centric method on Akk streams to manually trigger
   * backpressure and that is called throttling.
   */

  import scala.concurrent.duration._

  // this is a composite component that emits at most
  // 2 elements per second
  /*fastSource.throttle(2, 1.second)
    .runWith(Sink.foreach(println))*/
}
