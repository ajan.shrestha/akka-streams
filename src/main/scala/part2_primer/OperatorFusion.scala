package part2_primer

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}

object OperatorFusion extends App {

  /*
   * Goal
   *  - understand how stream components run on the same actor
   *  - how to introduce asynchronous boundaries between stream components
   */

  implicit private val system: ActorSystem = ActorSystem("OperatorFusion")

  private val simpleSource = Source(1 to 1000)
  private val simpleFlow = Flow[Int].map(_ + 1)
  private val simpleFlow2 = Flow[Int].map(_ * 10)
  private val simpleSink = Sink.foreach[Int](println)

  // this runs on the SAME ACTOR
  // Akka stream components are based on actors
  /*simpleSource.via(simpleFlow).via(simpleFlow2).to(simpleSink).run()*/
  // operator/component FUSION - components connected via,viaMat,to,toMat methods

  // Operator fusion mechanism is something that Akka streams does
  // by default behind the scenes, so that it improves throughput in general.
  // It's as if the below code is executed ~ equivalent behaviour
  /*
  private class SimpleActor extends Actor {
    override def receive: Receive = {
      case x: Int =>
        // flow operations
        val x2 = x + 1
        val y = x2 * 10
        // sink operation
        println(y)
    }
  }
  private val simpleActor = system.actorOf(Props[SimpleActor])
  (1 to 1000).foreach(simpleActor ! _)
  */
  // the end result with such Akka stream is that a single CPU core
  // will be used for the complete processing of every single element
  // throughout the entire flow, much like a CPU core will be used for
  // complete processing of every single element in the message handler
  // for the simple actor.

  // Operator Fusion causes more harm than good,
  // if the operations are time expensive.

  // complex flows:
  private val complexFlow = Flow[Int].map { x =>
    // simulation a long computation
    Thread.sleep(1000)
    x + 1
  }
  private val complexFlow2 = Flow[Int].map { x =>
    // simulation a long computation
    Thread.sleep(1000)
    x * 10
  }

  // simpleSource.via(complexFlow).via(complexFlow2).to(simpleSink).run()

  // When operators are expensive, it's worth making them run separately
  // in parallel on different actors.

  // async boundary - Akka Streams API to break operator fusion
  // pipeline expensive operations and run them in parallel
  // can introduce as much async boundary as we like
  /*simpleSource.via(complexFlow).async // runs on one actor
    .via(complexFlow2).async // runs on another actor
    .to(simpleSink) // runs on a third actor
    .run()*/

  /*
   *  [*] -> [*] -> [*]-> [*] default => operator fusion
   *
   * [*] -> [*]   |  -> [*]   |  -> [*]
   *              |           | actor 3
   *      actor 1 _           |
   *                  actor 2 _
   *
   * An async boundary contains
   *  - everything from the previous boundary(if any)
   *  - everything between the previous boundary and this boundary
   *
   * Communication based on actor messages
   *  - asynchronous actor messages
   *
   * Best when: individual operations are expensive
   * Avoid when: operations are comparable with a message pas
   *
   * Ordering guarantees
   */

  // ordering guarantees
  // fully deterministic and guaranteed order
  /*Source(1 to 3)
    .map(element => {
      println(s"Flow A: $element")
      element
    })
    .map(element => {
      println(s"Flow B: $element")
      element
    })
    .map(element => {
      println(s"Flow C: $element")
      element
    })
    .runWith(Sink.ignore)*/

  // guarantees relative order of the elements going inside every step of the Akka stream

  Source(1 to 3)
    .map(element => {
      println(s"Flow A: $element")
      element
    }).async
    .map(element => {
      println(s"Flow B: $element")
      element
    }).async
    .map(element => {
      println(s"Flow C: $element")
      element
    }).async
    .runWith(Sink.ignore)

}
