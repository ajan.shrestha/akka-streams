package part3_graphs

import akka.actor.ActorSystem
import akka.stream.{BidiShape, ClosedShape}
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}

object BidirectionalFlows extends App {
  /*
   * Goal
   *  Flows that go both ways: Bidirectional flow
   *    - a composite component that takes these two flows
   *      - a flow that transforms elements from type A to type B and
   *      - a flow that goes backwards from type B to type B
   *
   *          |     |
   *    A ->  | [ ] | -> B
   *    A <-  | [ ] | <- B
   *          | [ ] |
   *
   */

  implicit private val system: ActorSystem = ActorSystem("BidirectionalFlows")

  /*
   * Example: cryptography
   *  Caesers' cipher
   */

  private def encrypt(n: Int)(string: String) = string.map(char => (char + n).toChar)

  private def decrypt(n: Int)(string: String) = string.map(char => (char - n).toChar)
  // these functions often stay in the same place
  // in terms of Akka streams: same component -> bidi flow

  private val bidiCryptoStaticGraph = GraphDSL.create() { implicit builder =>
    val encryptionFlowShape = builder.add(Flow[String].map(encrypt(3)))
    val decryptionFlowShape = builder.add(Flow[String].map(decrypt(3)))

    // BidiShape(encryptionFlowShape.in, encryptionFlowShape.out, decryptionFlowShape.in, decryptionFlowShape.out)
    BidiShape.fromFlows(encryptionFlowShape, decryptionFlowShape)
  }

  private val unencryptedString = List("akka", "is", "awesome", "testing", "bidirectional", "flows")
  private val unencryptedSource = Source(unencryptedString)
  private val encryptedSource = Source(unencryptedString.map(encrypt(3)))

  private val cryptoBidiGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val unencryptedSourceShape = builder.add(unencryptedSource)
      val encryptedSourceShape = builder.add(encryptedSource)
      val bidi = builder.add(bidiCryptoStaticGraph)
      val encryptedSinkShape = builder.add(Sink.foreach[String](string => println(s"Encrypted: $string")))
      val decryptedSinkShape = builder.add(Sink.foreach[String](string => println(s"Decrypted: $string")))

      unencryptedSourceShape ~> bidi.in1
      bidi.out1 ~> encryptedSinkShape
      decryptedSinkShape <~ bidi.out2
      bidi.in2 <~ encryptedSourceShape

      ClosedShape
    }
  )

  cryptoBidiGraph.run()

  /*
   * Use-case:
   *  - Reversible operations
   *    - encrypting/decrypting
   *    - encoding/decoding
   *    - serializing/deserializing
   */
}
