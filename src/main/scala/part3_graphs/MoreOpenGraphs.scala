package part3_graphs

import akka.actor.ActorSystem
import akka.stream.{ClosedShape, FanOutShape2, UniformFanInShape}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source, ZipWith}

import java.util.Date

object MoreOpenGraphs extends App {
  /*
   * Goal
   *  More complex shapes with GraphDSL
   *  - fan-out
   *  - fan-in
   *  - uniform/non-uniform
   *    - uniform
   *      - called so as all of the inputs receive elements of
   *        the same type and the same goes for outputs
   */

  implicit private val system: ActorSystem = ActorSystem("MoreOpenGraphs")

  /*
   * Example" Max3 operator
   *  - 3 inputs of type int
   *  - the maximum of the 3
   *
   *          | Zipwith     |
   *  [] ->   | []          |
   *  [] ->   | [] -> []    | ->
   *  [] ->   | ----> [] -> |
   *          |             |
   */

  // step 1
  private val max3StaticGraph = GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._

    // step 2  - define aux SHAPES
    val max1 = builder.add(ZipWith[Int, Int, Int]((a, b) => Math.max(a, b)))
    val max2 = builder.add(ZipWith[Int, Int, Int]((a, b) => Math.max(a, b)))

    // step 3
    max1.out ~> max2.in0

    // step 4
    UniformFanInShape(max2.out, max1.in0, max1.in1, max2.in1)
  }

  private val source1 = Source(1 to 10)
  private val source2 = Source(1 to 10).map(_ => 5)
  private val source3 = Source((1 to 10).reverse)

  private val maxSink = Sink.foreach[Int](x => println(s"Max is :$x"))

  // step 1
  private val max3RunnableGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // step 2 - declare SHAPES
      val max3Shape = builder.add(max3StaticGraph)

      // step 3 - tie
      source1 ~> max3Shape.in(0)
      source2 ~> max3Shape.in(1)
      source3 ~> max3Shape.in(2)
      max3Shape.out ~> maxSink

      // step 4
      ClosedShape
    }
  )

  // max3RunnableGraph.run()

  // same for UniformFanOutShape

  /*
   * Non-uniform fan out shape
   *  - each input and output can deal with elements
   *    of totally different types
   *
   * Assume: Anti-money laundering application
   *  Processing bank transactions
   *    Txn Suspicious if amount > 10000
   *
   *  Stream component for txns
   *    - output1: let the transaction go through
   *    - output2: suspicious txn ids
   */

  private case class Transaction(id: String, source: String, recipient: String, amount: Int, date: Date)

  private val transactionSource = Source(List(
    Transaction("12345678", "Paul", "Jim", 100, new Date),
    Transaction("12345679", "Daniel", "Jim", 100000, new Date),
    Transaction("12345680", "Jim", "Alice", 7000, new Date)
  ))

  private val bankProcessor = Sink.foreach[Transaction](println)
  private val suspiciousAnalysisService = Sink.foreach[String](txnId =>
    println(s"Suspicious transaction ID: $txnId"))

  // step 1
  private val suspiciousTxnStaticGraph = GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._

    // step 2 - define SHAPES
    val broadcast = builder.add(Broadcast[Transaction](2))
    val suspiciousTxnFilter = builder.add(Flow[Transaction].filter(txn => txn.amount > 10000))
    val txnIdExtractor = builder.add(Flow[Transaction].map[String](txn => txn.id))

    // step 3 - tie SHAPES
    broadcast.out(0) ~> suspiciousTxnFilter ~> txnIdExtractor

    // step 4
    new FanOutShape2(broadcast.in, broadcast.out(1), txnIdExtractor.out)
  }

  // step 1
  private val suspiciousTxnRunnableGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // step 2
      val suspiciousTxnShape = builder.add(suspiciousTxnStaticGraph)

      // step 3
      transactionSource ~> suspiciousTxnShape.in
      suspiciousTxnShape.out0 ~> bankProcessor
      suspiciousTxnShape.out1 ~> suspiciousAnalysisService

      // step 4
      ClosedShape
    }
  )

  suspiciousTxnRunnableGraph.run()
}
