package part3_graphs

import akka.actor.ActorSystem
import akka.stream.{ClosedShape, FlowShape, SinkShape, SourceShape}
import akka.stream.scaladsl.{Broadcast, Concat, Flow, GraphDSL, RunnableGraph, Sink, Source}

object OpenGraphs extends App {

  implicit private val system: ActorSystem = ActorSystem("OpenGraphs")

  /*
   * A composite source that concatenates 2 sources
   *  - emits ALL the elements from the first source
   *  - then ALL the elements from the second
   */
  private val firstSource = Source(1 to 10)
  private val secondSource = Source(42 to 1000
  )
  /*
   * Complex source
   */
  // step 1
  private val sourceGraph = Source.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // step 2: declaring components
      // concat:
      //  takes all the elements from its first input and pushes them out, then
      //  all the elements from its second input and pushes them out and so on and so forth.
      val concat = builder.add(Concat[Int](2))

      // step 3: tying them together
      firstSource ~> concat
      secondSource ~> concat

      // step 4
      SourceShape(concat.out)
    }
  )
  /*
   *  |              [      ] |
   *  | [source1] -> [      ] |
   *  | [source2] -> [concat] |->
   *  |              [      ] |
   */

  // sourceGraph.to(Sink.foreach(println)).run()

  /*
   * Complex sink
   */
  private val sink1 = Sink.foreach[Int] { x => println(s"Meaningful think 1: $x") }
  private val sink2 = Sink.foreach[Int] { x => println(s"Meaningful think 2: $x") }

  // step 1
  private val sinkGraph = Sink.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // step 2 - add a broadcast
      val broadcast = builder.add(Broadcast[Int](2))

      // step 3 - tie components together
      broadcast ~> sink1
      broadcast ~> sink2

      // step 4
      SinkShape(broadcast.in)
    }
  )

  // firstSource.to(sinkGraph).run()

  /*
   * Challenge - complex flow?
   * Write your own flow that's composed of two other flows
   *  - one that adds 1 to a number
   *  - one that does number * 10
   */
  private val incrementer = Flow[Int].map(_ + 1)
  private val multiplier = Flow[Int].map(_ * 10)

  // step 1
  private val flowGraph = Flow.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // everything operates on SHAPES
      // step 2: define auxiliary SHAPES
      val incrementerShape = builder.add(incrementer)
      val multiplierShape = builder.add(multiplier)

      // step 3: connect the SHAPES
      // only shapes can be linked
      // incrementer ~> multiplier
      incrementerShape ~> multiplierShape

      // step 4
      FlowShape(incrementerShape.in, multiplierShape.out) // SHAPE
    } // static graph
  ) // component

  firstSource.via(flowGraph).to(Sink.foreach(println)).run()

  /*
   * Exercise: flow from a sink and a source?
   *
   *  |                         |
   *  | -> [ source ] [ sink ]  | ->
   *  |                         |
   */
  private def fromSinkAndSource[A, B](sink: Sink[A, _], source: Source[B, _]): Flow[A, B, _] = {
    // step 1
    Flow.fromGraph(
      GraphDSL.create() { implicit builder =>
        // step 2: declare the SHAPES
        val sourceShape = builder.add(source)
        val sinkShape = builder.add(sink)

        // step 3:

        // step 4 - return the shape
        FlowShape(sinkShape.in, sourceShape.out)
      }
    )
  }

  private val f = Flow.fromSinkAndSourceCoupled(Sink.foreach[String](println), Source(1 to 10))
}
