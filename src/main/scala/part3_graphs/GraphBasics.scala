package part3_graphs

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Balance, Broadcast, Flow, GraphDSL, Merge, RunnableGraph, Sink, Source, Zip}

object GraphBasics extends App {
  /*
   * Goal
   *  - write complex Akka Streams graphs
   *  - familiarize with the Graph DSL(Domain Specific Language)
   *  - introduce new components
   *    - non-linear components:
   *      - fan-out(single i/p & multiple o/p)
   *      - fan-in(multiple i/p & single o/p)
   */

  implicit private val system: ActorSystem = ActorSystem("GraphBasics")

  // Assume source of integers
  private val input = Source(1 to 1000)
  // Assume two hard computations
  private val incrementer = Flow[Int].map(x => x + 1)
  private val multiplier = Flow[Int].map(x => x * 10)
  // Like to execute both of these flows in parallel
  // and merge back the result in tuple/pair
  private val output = Sink.foreach[(Int, Int)](println)

  // In order to achieve, parallel computation and merge
  // we need to construct a complex graph with a
  // fan out operator which feeds i/p into multiple o/ps (Source => Flow)
  // and a fan in operator which feeds these o/ps into i/ps (Flow => Sink)

  /*
   * graph is of type Runnable graph
   *  which is the kind of object that can be executed and materialized
   *
   * GraphDSL.create -> create static or inert graph
   *
   * creating a graph shape
   *  then from the shape we're creating an inert, static graph
   *    from the static graph we create a runnable graph
   *      then we're finally able to run the graph and materialized it
   */

  /*
  // step 1 - setting up the fundamentals
  private val graph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
      import GraphDSL.Implicits._ // brings some nice operators into scope
      // must return a shape object
    } // this expression must be a graph (inert/static graph)
  ) // runnable graph
  graph.run() // run the graph and materialize it
  */

  /*// step 1 - setting up the fundamentals
  private val graph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
      import GraphDSL.Implicits._ // brings some nice operators into scope

      // step 2 - add the necessary components of this graph
      /*
       * source will broadcast its elements to both flows
       *  for this a special component is required that forks out
       *  every single element to two outputs
       *  feeding into the 2 flows, respectively.
       *
       * Output of the flows will be merged back into a single
       *  output and paired as a tuple and fed into sink
       */
      val broadcast = builder.add(Broadcast[Int](2)) // fan-out operator
      val zip = builder.add(Zip[Int, Int]) // fan-in operator

      // must return a shape object
    } // this expression must be a graph (inert/static graph)
  ) // runnable graph
  graph.run() // run the graph and materialize it*/


  /*// step 1 - setting up the fundamentals
  private val graph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
      import GraphDSL.Implicits._ // brings some nice operators into scope

      // step 2 - add the necessary components of this graph
      /*
       * source will broadcast its elements to both flows
       *  for this a special component is required that forks out
       *  every single element to two outputs
       *  feeding into the 2 flows, respectively.
       *
       * Output of the flows will be merged back into a single
       *  output and paired as a tuple and fed into sink
       */
      val broadcast = builder.add(Broadcast[Int](2)) // fan-out operator
      val zip = builder.add(Zip[Int, Int]) // fan-in operator

      // step 3 - tying up the components
      input ~> broadcast // input feeds into broadcast

      broadcast.out(0) ~> incrementer ~> zip.in0
      broadcast.out(1) ~> multiplier ~> zip.in1

      zip.out ~> output

      // must return a shape object
    } // this expression must be a graph (inert/static graph)
  ) // runnable graph
  graph.run() // run the graph and materialize it*/


  // step 1 - setting up the fundamentals
  private val graph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] => // builder = MUTABLE data structure
      import GraphDSL.Implicits._ // brings some nice operators into scope

      // step 2 - add the necessary components of this graph
      /*
       * source will broadcast its elements to both flows
       *  for this a special component is required that forks out
       *  every single element to two outputs
       *  feeding into the 2 flows, respectively.
       *
       * Output of the flows will be merged back into a single
       *  output and paired as a tuple and fed into sink
       */
      val broadcast = builder.add(Broadcast[Int](2)) // fan-out operator
      val zip = builder.add(Zip[Int, Int]) // fan-in operator

      // step 3 - tying up the components
      input ~> broadcast // input feeds into broadcast

      broadcast.out(0) ~> incrementer ~> zip.in0
      broadcast.out(1) ~> multiplier ~> zip.in1

      zip.out ~> output

      /*
       * everything above mutates the builder
       *  - setting up all the connections,
       *  - adding all these shapes, and
       *  - tying them all together
      */

      // step 4 - return a closed shape
      ClosedShape // FREEZE the builder's shape
      // this shape will be used to create the graph
      // must return a shape object
    } // this expression must be a graph (inert/static graph)
  ) // runnable graph
  /*
   *
   *              [           ] -> [ Flow 1 ] -> [     ]
   *  [Source] -> [ Broadcast ]                  [ Zip ] -> [ Sink ]
   *              [           ] -> [ Flow 2 ] -> [     ]
   */

  // graph.run() // run the graph and materialize it

  /*
   * exercise 1: feed a source into 2 sinks at the same time (hint: use a broadcast)
   */
  private val firstSink = Sink.foreach[Int](x => println(s"First sink : $x"))
  private val secondSink = Sink.foreach[Int](x => println(s"Second sink : $x"))
  // step 1
  private val sourceToTwoSinkGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // step 2 - declaring the components
      val broadcast = builder.add(Broadcast[Int](2))

      // step 3 - tying up the components
      /*
      input ~> broadcast
      broadcast.out(0) ~> firstSink
      broadcast.out(1) ~> secondSink
      */
      input ~> broadcast ~> firstSink // implicit port numbering
      broadcast ~> secondSink

      // step 4
      ClosedShape
    }
  )
  /*
   *
   *              [           ] -> [ Sink 1 ]
   *  [Source] -> [ Broadcast ]
   *              [           ] -> [ Sink 2 ]
   */
  // sourceToTwoSinkGraph.run()

  /*
   * Exercise 2: balance
   *
   *  [ fast source ] -> [       ]    [         ] -> [ sink 1 ]
   *                     [ merge ] -> [ balance ]
   *  [ slow source ] -> [       ]    [         ] -> [ sink 2 ]
   *
   * merge:
   *  - fan-in shape
   *  - a merge component will take any element out of any one of its i/ps and
   *    it'll just push it out of its o/p
   *
   * balance:
   *  - fan-out shape
   *  - a balance component will distribute the i/p elements equally
   *    through its o/ps
   */

  import scala.concurrent.duration._

  private val fastSource = input.throttle(5, 1.second)
  private val slowSource = input.throttle(2, 1.second)
  /*private val sink1 = Sink.foreach[Int](x => println(s"Sink 1: $x"))
  private val sink2 = Sink.foreach[Int](x => println(s"Sink 2: $x"))*/
  private val sink1 = Sink.fold[Int, Int](0)((count, _) => {
    println(s"Sink 1 number of elements: $count")
    count + 1
  })
  private val sink2 = Sink.fold[Int, Int](0)((count, _) => {
    println(s"Sink 2 number of elements: $count")
    count + 1
  })

  // step 1
  private val balanceGraph = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // step 2 - declare components
      val merge = builder.add(Merge[Int](2))
      val balance = builder.add(Balance[Int](2))

      // step 3 - tie them up
      fastSource ~> merge ~> balance ~> sink1
      slowSource ~> merge
      balance ~> sink2

      // step 4
      ClosedShape
    }
  )
  /*
   * Balance graph does in total
   *  - it takes two very different sources emitting elements
   *    at different rates, and it evens out the rate of
   *    production of the elements in between these two sources and
   *    spits them out equally in between two sinks.
   *
   * LOAD BALANCER
   */
  balanceGraph.run()
}
