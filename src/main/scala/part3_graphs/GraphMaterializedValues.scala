package part3_graphs

import akka.actor.ActorSystem
import akka.stream.{FlowShape, SinkShape}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Sink, Source}

import scala.concurrent.Future
import scala.util.{Failure, Success}

object GraphMaterializedValues extends App {
  /*
   * Goal
   *  Expose materialized values in components built with GraphDSL
   */

  implicit private val system: ActorSystem = ActorSystem("GraphMaterializedValues")

  /*
   * Materialized value
   *  - value that a component exposed when it's run in a graph
   *  - it can be anything
   *    - it might not even be connected to the actual data that flows
   *      through the component.
   */

  private val wordSource = Source(List("Akka", "is", "awesome", "rock", "the", "jvm"))
  private val printer = Sink.foreach[String](println)
  private val counter = Sink.fold[Int, String](0)((count, _) => count + 1)
  // Future[Int] materialized value of the sink

  /*
   * A composite component (sink)
   *  - prints out all string which are lowercase
   *  - COUNTS the strings that are short (< 5 chars)
   */

  /*// step 1
  private val complexWordSink = Sink.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      // step 2 - SHAPES
      val broadcast = builder.add(Broadcast[String](2))
      val lowercaseFilter = builder.add(Flow[String].filter(word => word == word.toLowerCase))
      val shortStringFilter = builder.add(Flow[String].filter(_.length < 5))

      // step 3 - connections
      broadcast ~> lowercaseFilter ~> printer
      broadcast ~> shortStringFilter ~> counter

      // step 4 - the shape
      SinkShape(broadcast.in)
    }
  )*/

  /*// step 1
  // now expose the materialized value along with the shape
  private val complexWordSink = Sink.fromGraph(
    /*
     * GraphDSL.create takes in parameter - actual components
     *  On adding parameter, the following function, instead of
     *  being a function from a builder to the shape, it will be
     *  a higher order function between a builder and a function that
     *  takes the shape of the parameter that is inserted and the
     *  shape that is required.
     */
    // step I - pass in the right component whose materialized value we care about
    GraphDSL.createGraph(counter) { implicit builder =>
      // step II - then changed the function signature on the right hand side
      counterShape =>
        import GraphDSL.Implicits._

        // step 2 - SHAPES
        val broadcast = builder.add(Broadcast[String](2))
        val lowercaseFilter = builder.add(Flow[String].filter(word => word == word.toLowerCase))
        val shortStringFilter = builder.add(Flow[String].filter(_.length < 5))

        // step 3 - connections
        broadcast ~> lowercaseFilter ~> printer
        // step III - finally used the shape where we originally used the component
        broadcast ~> shortStringFilter ~> counterShape

        // step 4 - the shape
        SinkShape(broadcast.in)
    }
  )*/

  // step 1
  // now expose multiple materialized value along with the shape
  private val complexWordSink = Sink.fromGraph(
    /*
     * GraphDSL.create takes in parameter - actual components
     *  On adding parameter, the following function, instead of
     *  being a function from a builder to the shape, it will be
     *  a higher order function between a builder and a function that
     *  takes the shape of the parameter that is inserted and the
     *  shape that is required.
     */
    // step I - pass in the components whose materialized values we care about
    GraphDSL.createGraph(printer, counter)(
      // step II - compose their materialized values ina function
      (printerMatValue, counterMatValue) => counterMatValue) {
      implicit builder =>
        // step III - then pass in their shapes in the HOF
        (printerShape, counterShape) =>
          import GraphDSL.Implicits._

          // step 2 - SHAPES
          val broadcast = builder.add(Broadcast[String](2))
          val lowercaseFilter = builder.add(Flow[String].filter(word => word == word.toLowerCase))
          val shortStringFilter = builder.add(Flow[String].filter(_.length < 5))

          // step 3 - connections
          // step IV - finally use their shape in the composite components implementation.
          broadcast ~> lowercaseFilter ~> printerShape
          // step IV - finally use their shape in the composite components implementation.
          broadcast ~> shortStringFilter ~> counterShape

          // step 4 - the shape
          SinkShape(broadcast.in)
    }
  )

  import system.dispatcher

  private val shortStringsCountFuture = wordSource.toMat(complexWordSink)(Keep.right).run()
  shortStringsCountFuture.onComplete {
    case Success(count) => println(s"The total number of short strings is: $count")
    case Failure(exception) => println(s"The count of short strings failed: $exception")
  }

  /*
   * Exercise
   */
  private def enhancedFlow[A, B](flow: Flow[A, B, _]): Flow[A, B, Future[Int]] = {
    val counterSink = Sink.fold[Int, B](0)((count, _) => count + 1)

    Flow.fromGraph(
      GraphDSL.createGraph(counterSink) { implicit builder =>
        counterSinkShape =>
          import GraphDSL.Implicits._

          val broadcast = builder.add(Broadcast[B](2))
          val originalFlowShape = builder.add(flow)

          originalFlowShape ~> broadcast ~> counterSinkShape

          FlowShape(originalFlowShape.in, broadcast.out(1))
      }
    )
  }
  // Hint: use a broadcast and a Sink.fold

  private val simpleSource = Source(1 to 42)
  private val simpleFlow = Flow[Int].map(x => x)
  private val simpleSink = Sink.ignore

  private val enhancedFlowCountFuture = simpleSource
    .viaMat(enhancedFlow(simpleFlow))(Keep.right)
    .toMat(simpleSink)(Keep.left)
    .run()

  enhancedFlowCountFuture.onComplete {
    case Success(count) => println(s"$count elements went through the enhanced flow")
    case _ => println("Something failed")
  }
}
