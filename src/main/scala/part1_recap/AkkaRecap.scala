package part1_recap

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor.{Actor, ActorLogging, ActorSystem, OneForOneStrategy, PoisonPill, Props, Stash, SupervisorStrategy}
import akka.util.Timeout

object AkkaRecap extends App {

  private class SimpleActor extends Actor with ActorLogging with Stash {
    override def receive: Receive = {
      case "createChild" =>
        val childActor = context.actorOf(Props[SimpleActor], "myChild")
        childActor ! "hello"
      case "stashThis" =>
        stash()
      case "change handler now" =>
        unstashAll()
        context.become((anotherHandler))
      case "change" => context.become(anotherHandler)
      case message => println(s"I received: $message")
    }

    private def anotherHandler: Receive = {
      case message => println(s"In another receive handler: $message")
    }

    override def preStart(): Unit = {
      log.info("I'm starting")
    }

    override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
      case _: RuntimeException => Restart
      case _ => Stop
    }
  }

  // actor encapsulation
  private val system = ActorSystem("AkkaRecap")
  // #1: you can only instantiate an actor through the actor system
  private val actor = system.actorOf(Props[SimpleActor], "simpleActor")
  // #2: communicate with actor via sending messages
  actor ! "hello"

  /*
   * - messages are sent asynchronously
   * - many actors (in the millions) can share a few dozen threads
   * - each message is processed/handled ATOMICALLY
   * - no need for locks
   */

  // changing actor behavior + stashing
  // actors can spawn other actors
  // guardians -> Akka 3 top level parent actors
  // /system
  // /user -> parent of every single actor that we create
  // / = root -> parent of all

  // actors have a defined lifecycle:
  // they can be started, stopped, suspended, resumed, restarted

  // stopping actors -  context.stop
  actor ! PoisonPill // handled in a separate mailbox

  // logging
  // supervision

  // configure Akka infrastructure: dispatchers, routers, mailboxes

  // schedulers

  import system.dispatcher
  import scala.concurrent.duration._

  system.scheduler.scheduleOnce(2.seconds) {
    actor ! "delayed happy birthday"
  }

  // Akka patterns including FSM + ask pattern

  import akka.pattern.ask

  implicit private val timeout: Timeout = Timeout(3.seconds)

  private val future = actor ? "question" // first reply withing 3 seconds else fail

  // the pipe pattern

  import akka.pattern.pipe

  private val anotherActor = system.actorOf(Props[SimpleActor], "anotherSimpleActor")
  future.mapTo[String].pipeTo(anotherActor) // on completion message sent to anotherActor
}
