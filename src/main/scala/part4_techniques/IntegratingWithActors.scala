package part4_techniques

import akka.Done
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.stream.{CompletionStrategy, OverflowStrategy}
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.util.Timeout

import scala.concurrent.duration._

object IntegratingWithActors extends App {
  /*
   * Goal
   *  Make Actors interact with Akka streams (can be inserted anywhere in Akka stream)
   *    - process elements in streams
   *    - act as a source
   *    - act as a destination
   */

  implicit private val system: ActorSystem = ActorSystem("IntegratingWithActors")

  private class SimpleActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case s: String =>
        log.info(s"Just received a string: $s")
        sender() ! s"$s$s"
      case n: Int =>
        log.info(s"Just received a number: $n")
        sender() ! (2 * n)
      case _ =>
    }
  }

  private val simpleActor = system.actorOf(Props[SimpleActor], "simpleActor")

  private val numbersSource = Source(1 to 10)

  /*actor as a flow*/
  implicit val timeout: Timeout = Timeout(2.seconds)
  // parallelism factor
  //  how many messages can be in this actor's mailbox at any one time
  //  before the actor starts back pressuring.
  private val actorBasedFlow = Flow[Int].ask[Int](parallelism = 4)(simpleActor)

  /*
  numbersSource.via(actorBasedFlow).to(Sink.ignore).run()
  numbersSource.ask[Int](parallelism = 4)(simpleActor).to(Sink.ignore).run() // equivalent
  */

  /*Actor as a source*/
  private val actorPoweredSource = Source.actorRef(
    completionMatcher = {
      case Done =>
        // complete stream immediately if we send it Done
        CompletionStrategy.immediately
    },
    // never fail the stream because of a message
    failureMatcher = PartialFunction.empty,
    bufferSize = 10,
    overflowStrategy = OverflowStrategy.dropHead
  )
  private val materializedActorRef = actorPoweredSource.to(
    Sink.foreach[Int](number => println(s"Actor powered flow got number: $number"))).run()

  materializedActorRef ! 10
  materializedActorRef ! 20

  // terminating the stream
  // The stream completes successfully with the following message
  materializedActorRef ! Done
  // materializedActorRef ! 11 // this is ignored

  /*
   * Actor as a destination/sink
   *  An actor powered sink will need to support some special signals.
   *    It will need to support an initialization message which will be
   *    sent first by whichever component ends up connected to this
   *    actor powered sink.
   *  - an init message
   *  - an ack message to confirm the reception
   *  - a complete message
   *  - a function to generate a message in case the stream throw an exception
   */
  private case object StreamInit

  private case object StreamAck

  private case object StreamComplete

  private case class StreamFail(ex: Throwable)

  private class DestinationActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case StreamInit =>
        log.info("Stream initialized")
        sender() ! StreamAck
      case StreamComplete =>
        log.info("Stream complete")
        context.stop(self)
      case StreamFail(ex) =>
        log.warning(s"Stream failed: $ex")
      case message =>
        log.info(s"Message $message has come to its final resting point.")
        sender() ! StreamAck // in it's absence backpressure will be applied
    }
  }

  private val destinationActor = system.actorOf(Props[DestinationActor], "destinationActor")

  private val actorPoweredSink = Sink.actorRefWithBackpressure(
    destinationActor,
    onInitMessage = StreamInit,
    ackMessage = StreamAck,
    onCompleteMessage = StreamComplete,
    onFailureMessage = throwable => StreamFail(throwable) // optional
  )

  Source(1 to 10).to(actorPoweredSink).run()
}
