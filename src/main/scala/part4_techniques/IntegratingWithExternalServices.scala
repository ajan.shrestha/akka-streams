package part4_techniques

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.dispatch.MessageDispatcher
import akka.pattern.ask
import akka.stream.scaladsl.{Sink, Source}
import akka.util.Timeout

import java.util.Date
import scala.concurrent.duration._
import scala.concurrent.Future

object IntegratingWithExternalServices extends App {
  /*
   * Goal
   *  Akka Streams & Futures
   *    - Important for integrating with external services or asynchronous computation
   *
   * Recap
   *  Flows that call external services
   *    mySource.mapAsync(parallelism = 5)(element => MyExternalService.externalCall(element))
   *
   *  Useful when the services are asynchronous
   *    - the Futures are evaluated in parallel
   *    - the relative order of elements is maintained
   *    - a lagging Future will stall the entire stream
   *
   *  mySource.mayAsyncUnordered(parallelism = 5)(...) // if ordering is irrelevant; performance boost
   *
   *  Evaluate the Futures on their own ExecutionContext. (best practice)
   *
   *  MayAsync works great with asking actors
   *    mySource.mayAsync(parallelism = 5)(mySource ? _)
   */

  implicit private val system: ActorSystem = ActorSystem("IntegratingWithExternalServices")
  // import system.dispatcher // not recommended in practice for mayAsync
  implicit val dispatcher: MessageDispatcher = system.dispatchers.lookup("dedicated-dispatcher")

  // generalize external service / asynchronous computation as a future
  private def genericExtService[A, B](element: A): Future[B] = ???

  /*
   * Example: simplified PagerDuty
   *  - a service to manage on call software engineers
   *  - in case there is an issue or something breaks in the code (in production),
   *    an alert is issued and the on call engineer is emailed or otherwise notified
   *    via some kind of text or phone call.
   *
   * Design an Akka stream that deals with this kind of events and alerts.
   * Integrate with a fictitious external asynchronous API.
   */
  private case class PagerEvent(application: String, description: String, date: Date)

  private val eventSource = Source(List(
    PagerEvent("AkkaInfra", "Infrastructure broke", new Date),
    PagerEvent("FastDataPipeline", "Illegal elements in the data pipeline", new Date),
    PagerEvent("AkkaInfra", "A service stopped responding", new Date),
    PagerEvent("SuperFrontend", "A button doesn't work", new Date),
  ))

  private object PagerService {
    private val engineers = List("Daniel", "John", "Lady Gaga")
    private val emails = Map(
      "Daniel" -> "daniel@rockthejvm.com",
      "John" -> "john@rockthejvm.com",
      "Lady Gaga" -> "ladygaga@rockthejvm.com",
    )

    def processEvent(pagerEvent: PagerEvent): Future[String] = Future {
      val engineerIndex = (pagerEvent.date.toInstant.getEpochSecond / (24 * 3600)) % engineers.length
      val engineer = engineers(engineerIndex.toInt)
      val engineerEmail = emails(engineer)

      // page the engineer
      println(s"Sending engineer $engineerEmail a high priority notification: $pagerEvent")
      Thread.sleep(1000)

      // return the email that was paged
      engineerEmail
    }
  }

  private val infraEvents = eventSource.filter(_.application == "AkkaInfra")
  // parallelism -> determines the number of future that can be run at the same time
  // if one future fails, the whole stream fails
  // mapAsyncUnordered -> performance gain without order
  // mapAsync -> has to wait for the future to complete to maintain order
  private val pagedEngineerEmails = infraEvents.mapAsync(parallelism = 4)(event => PagerService.processEvent(event))
  // guarantees the relative order of elements
  private val pagedEmailsSink = Sink.foreach[String](email => println(s"Successfully sent notification to $email"))

  // pagedEngineerEmails.to(pagedEmailsSink).run()


  /*
   * Asking actors as mapAsync works well with futures
   */
  private class PagerActor extends Actor with ActorLogging {
    private val engineers = List("Daniel", "John", "Lady Gaga")
    private val emails = Map(
      "Daniel" -> "daniel@rockthejvm.com",
      "John" -> "john@rockthejvm.com",
      "Lady Gaga" -> "ladygaga@rockthejvm.com",
    )

    private def processEvent(pagerEvent: PagerEvent): String = {
      val engineerIndex = (pagerEvent.date.toInstant.getEpochSecond / (24 * 3600)) % engineers.length
      val engineer = engineers(engineerIndex.toInt)
      val engineerEmail = emails(engineer)

      // page the engineer
      log.info(s"Sending engineer $engineerEmail a high priority notification: $pagerEvent")
      Thread.sleep(1000)

      // return the email that was paged
      engineerEmail
    }

    override def receive: Receive = {
      case pagerEvent: PagerEvent =>
        sender() ! processEvent(pagerEvent)
    }
  }

  implicit val timeout: Timeout = Timeout(3.seconds)
  private val pagerActor = system.actorOf(Props[PagerActor], "pagerActor")
  private val alternativePagedEngineerEmails =
    infraEvents.mapAsync(parallelism = 4)(event => (pagerActor ? event).mapTo[String])
  alternativePagedEngineerEmails.to(pagedEmailsSink).run()

  // do not confused mapAsync with async (ASYNC boundary)
}
