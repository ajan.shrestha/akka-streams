package part4_techniques

import akka.actor.ActorSystem
import akka.stream.Supervision.{Resume, Stop}
import akka.stream.{ActorAttributes, RestartSettings}
import akka.stream.scaladsl.{RestartSource, Sink, Source}

import scala.concurrent.duration.DurationInt
import scala.util.Random

object FaultTolerance extends App {
  /*
   * Goal
   *  React to failure in streams
   */

  implicit private val system: ActorSystem = ActorSystem("FaultTolerance")

  // 1 - logging: monitoring elements, completion and failure
  private val faultySource = Source(1 to 10).map(e => if (e == 6) throw new RuntimeException else e)
  faultySource
    .log("trackingElements")
    .to(Sink.ignore)
  //.run()
  // elements: DEBUG - actual data going out of source
  // failures: ERROR
  // default loglevel: INFO

  // 2 - gracefully terminating a stream
  faultySource.recover {
    case _: RuntimeException => Int.MinValue
  }
    .log("gracefulSource")
    .to(Sink.ignore)
  //.run()

  // 3 - recover with another stream
  faultySource.recoverWithRetries(attempts = 3, {
    case _: RuntimeException => Source(90 to 99)
  })
    .log("recoverWithRetries")
    .to(Sink.ignore)
  //.run()

  // 4 - backoff supervision
  private val settings = RestartSettings(
    minBackoff = 1.second,
    maxBackoff = 30.seconds,
    randomFactor = 0.2
  )
  private val restartSource = RestartSource.withBackoff(settings)(() => {
    val randomNumber = new Random().nextInt(20)
    Source(1 to 10).map(elem => if (elem == randomNumber) throw new RuntimeException else elem)
  })

  restartSource
    .log("restartBackoff")
    .to(Sink.ignore)
  //.run()

  // 5 - supervision strategy
  private val numbers = Source(1 to 20)
    .map(n => if (n == 13) throw new RuntimeException("bad luck") else n)
    .log("supervision")

  private val supervisedNumbers = numbers.withAttributes(ActorAttributes.supervisionStrategy {
    /*
     * Resume = skips the faulty element
     * Stop = stops the stream
     * Restart = resume + clears internal state
    */
    case _: RuntimeException => Resume
    case _ => Stop
  })

  supervisedNumbers
    .to(Sink.ignore)
    .run()
}
