package part4_techniques

import akka.actor.ActorSystem
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source}

import java.util.Date
import scala.concurrent.duration.DurationInt

object AdvancedBackpressure extends App {
  /*
   * Goal
   *  Recap how to use buffers to control the rate of the flow of elements inside Akka stream
   *  Deal with interesting situations
   *    - Coping with an un-backpressure-able source by conflate
   *    - deal with a fast consumer by extrapolating/expanding
   */

  implicit private val system: ActorSystem = ActorSystem("AdvancedBackpressure")

  // control backpressure
  private val controlledFlow = Flow[Int].map(_ * 2).buffer(10, OverflowStrategy.dropHead)

  // private case class PagerEvent(description: String, date: Date)
  private case class PagerEvent(description: String, date: Date, nInstances: Int = 1)

  private case class Notification(email: String, pagerEvent: PagerEvent)

  private val events = List(
    PagerEvent("Service discovery failed", new Date),
    PagerEvent("Illegal elements in the data pipeline", new Date),
    PagerEvent("Number of HTTP 500 spiked", new Date),
    PagerEvent("A service stopped responding", new Date),
  )
  private val eventSource = Source(events)

  private val oncallEngineer = "daniel@rockthejvm.com" // a fast service for fetching on call emails

  private def sendEmail(notification: Notification): Unit =
    println(s"Dear ${notification.email}, you have an event: ${notification.pagerEvent}") // actually sends an email

  private val notificationSink = Flow[PagerEvent]
    .map(event => Notification(oncallEngineer, event))
    .to(Sink.foreach[Notification](sendEmail))

  // standard
  // eventSource.to(notificationSink).run()

  /*
   * Assume Notification sink, or the email service is slow for some reason.
   * In that case, the notification section normally try to backpressure the source.
   * This might cause issues because on the practical side, the relevant engineers
   * that should be paged by our service may not be paged on time.
   * And for Akka streams consideration, this event source may not actually be able
   * to backpressure, especially if it's timer based.
   * Timer based sources do not respond to backpressure.
   */

  /*
   * un-backpressure-able source
   *
   *  Solution
   *    - somehow aggregate the incoming events and create one single notification
   *      when we receive demand from the sink.
   *    - so instead of buffering the events on our flow here, and creating three
   *      notification for three different events, we can create a single notification
   *      for multiple events.
   *
   *  Method: conflate
   *    - acts like fold, it combines elements
   *    - it emits the result only when the downstream sends demand
   */

  private def sendEmailSlow(notification: Notification): Unit = {
    Thread.sleep(1000)
    println(s"Dear ${notification.email}, you have an event: ${notification.pagerEvent}") // actually sends an email
  }

  private val aggregateNotificationFlow = Flow[PagerEvent]
    .conflate((event1, event2) => {
      val nInstances = event1.nInstances + event2.nInstances
      PagerEvent(s"You have $nInstances events that require your attention", new Date, nInstances)
    })
    .map(resultingEvent => Notification(oncallEngineer, resultingEvent))

  /*eventSource
    .via(aggregateNotificationFlow).async
    .to(Sink.foreach[Notification](sendEmailSlow))
    .run()*/
  // alternative to backpressure

  /*
   * Slow producers: extrapolate/expand
   *
   *  - we have a fast consumer and a slow producer and
   *    there is always unmet demand from the consumer.
   *  - we can always extrapolate i.e. run a function from the last element
   *    emitted from upstream to compute further elements to be emitted downstream.
   */
  private val slowCounter = Source(LazyList.from(1)).throttle(1, 1.second)
  private val hungrySink = Sink.foreach[Int](println)
  // we can insert a special flow in between
  // extrapolate create this iterator only when there is unmet demand
  private val extrapolator = Flow[Int].extrapolate(element => Iterator.from(element))
  private val repeater = Flow[Int].extrapolate(element => Iterator.continually(element))

  // slowCounter.via(extrapolator).to(hungrySink).run()
  slowCounter.via(repeater).to(hungrySink).run()

  // works the same way as extrapolate with a twist
  // expand create this iterator at all times.
  private val expander = Flow[Int].expand(element => Iterator.from(element))
}
