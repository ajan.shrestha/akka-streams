# The official repository for the Rock the JVM Akka Streams with Scala course

This repository contains the code as seen on  [Rock the JVM's Akka Streams with Scala](https://www.udemy.com/akka-streams) course on Udemy. 
